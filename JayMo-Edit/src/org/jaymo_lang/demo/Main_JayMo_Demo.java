/*******************************************************************************
 * Copyright (C): 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.demo;

import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import org.jaymo_lang.demo.defs.DemoMainPanel;
import org.jaymo_lang.error.ErrorBaseDebug;
import org.jaymo_lang.model.App;
import org.jaymo_lang.parser.Parser_App;

import de.mn77.base.data.group.Group4;
import de.mn77.base.error.Err;
import de.mn77.base.event.Procedure;
import de.mn77.base.sys.Sys;
import de.mn77.base.thread.A_ParallelProcess;


/**
 * @author Michael Nitsche
 * @created 22.12.2019
 */
public class Main_JayMo_Demo {

	private App runningApp = null;


	private Thread runningThread = null;


	public static void main( final String[] args ) {
		javax.swing.SwingUtilities.invokeLater( () -> new Main_JayMo_Demo().createAndShowGUI() );
	}

	private void createAndShowGUI() {
		final Parser_App parser = new Parser_App();
		final String version = "JayMo-Demo  " + parser.getVersionString( false, true, true );

		final JFrame frame = new JFrame( version );
		frame.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );

		final DemoMainPanel panel = new DemoMainPanel( ( final Group4<String, JTextArea, JTextField, Procedure> g ) -> this.execute( parser, g ), () -> this.reset() );
		frame.add( panel );

		frame.pack();
//		frame.setSize(800, 400);
		frame.setSize( 1100, 500 );
		frame.setVisible( true );

		final ImageIcon img = new ImageIcon( this.getClass().getClassLoader().getResource( "jar/logo/jaymo_icon_256.png" ) );
		frame.setIconImage( img.getImage() );
		final int CTRL = Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx(); // InputEvent.CTRL_DOWN_MASK

		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher( e -> {
//			MOut.temp(e.getExtendedKeyCode(), e.getID(), e.getKeyChar(), e.getKeyCode(), e.getKeyLocation(), e.getModifiers(), e.getModifiersEx(), e.getSource());

			if( KeyEvent.KEY_RELEASED != e.getID() )
				return false;

			if( e.getKeyCode() == 81 && e.getModifiersEx() == CTRL ) // Strg+Q
				frame.dispose();

			if( e.getKeyCode() == 10 && e.getModifiersEx() == CTRL ) // Strg+Enter
				panel.execute();

			return false;
		} );
	}

	private void execute( final Parser_App parser, final Group4<String, JTextArea, JTextField, Procedure> g ) {
		final String sourcecode = g.o1;

		if( Main_JayMo_Demo.this.runningThread != null )
			return;
		this.runningThread = new A_ParallelProcess() {

			@Override
			protected void onFinished() {
				g.o4.execute();
				Main_JayMo_Demo.this.runningApp = null;
				Main_JayMo_Demo.this.runningThread = null;
			}

			@Override
			protected void process() {
				if( Main_JayMo_Demo.this.runningApp != null )
					return;

				App app = null;

				try {
//					final App app = parser.parseText(">strictSave");
					app = parser.parseText( ">strictWebstart" );
					parser.parseText( app, sourcecode );

					Main_JayMo_Demo.this.runningApp = app;
					// Empty output
					g.o2.setText( "" );
					g.o3.setText( "" );

					// Write output direct
					app.setOutputRedirection( ( final String s ) -> {
						g.o2.append( s );
						g.o2.setCaretPosition( g.o2.getText().length() ); // Scroll to the end of the text
//						g.o2.repaint();
					} );

//					app.describe();
					app.setNoHardExit();
					final String result = app.exec( null );
					if( !app.toBeTerminated() && result != null )
						g.o3.setText( result );
				}
				catch( final ErrorBaseDebug ce ) {

					if( app != null && !app.toBeTerminated() ) {
						g.o2.setText( ce.toInfo() );
						Err.show( ce );
					}
				}
			}

		};
	}

	@SuppressWarnings( "deprecation" )
	private void reset() {
		if( this.runningApp != null )
			//			MOut.print("Terminate");
			this.runningApp.terminate();

		Sys.sleep( 100 );

		if( this.runningThread != null )
			//			MOut.print("Interrupt", this.runningThread);
			this.runningThread.interrupt();

		Sys.sleep( 100 );

		try {
			if( this.runningThread != null )
				//				MOut.print("Stop", this.runningThread);
				this.runningThread.stop(); // Hard end
		}
		catch( final ThreadDeath td ) {}
	}

}
