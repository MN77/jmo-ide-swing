/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.dialog;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.edit.Main_JayMo_Edit;
import org.jaymo_lang.parser.Parser_App;

import de.mn77.base.error.Err;
import de.mn77.base.sys.file.Lib_Jar;


/**
 * @author Michael Nitsche
 * @created 18.03.2021
 */
public class Dialog_About {

	public static void main( final String[] args ) { // TEST
		final JFrame frame = new JFrame();
		Dialog_About.show( frame );
		frame.dispose();
	}

	public static void show( final Component parent ) {
		Err.ifNull( parent );
		final ImageIcon img = Lib_Jar.getImageIcon( "/jar/logo/jaymo-r_180x162.png" );

		String text = "";
		text += Main_JayMo_Edit.NAME + ": " + Main_JayMo_Edit.VERSION.toStringShort() + '\n';
		text += "JayMo: " + new Parser_App().getVersionString( false, false, true ) + '\n';
		text += "\n";
		text += "Copyright © " + JayMo.COPYRIGHT_YEAR + '\n' + JayMo.COPYRIGHT_NAME;

		final JTextPane jtext = new JTextPane();
		final StyledDocument doc = jtext.getStyledDocument();
		jtext.setDocument( doc );
		jtext.setAutoscrolls( false );
//		textarea.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1, false));
		jtext.setEditable( false );
		jtext.setMargin( new Insets( 10, 10, 10, 10 ) );

		final SimpleAttributeSet attrs = new SimpleAttributeSet();
		StyleConstants.setAlignment( attrs, StyleConstants.ALIGN_CENTER );
		doc.setParagraphAttributes( 0, doc.getLength() - 1, attrs, false );

		jtext.setText( text );
		jtext.setOpaque( false );

		final JLabel name = new JLabel( Main_JayMo_Edit.NAME, SwingConstants.CENTER );
		name.setPreferredSize( new Dimension( 250, 30 ) );

		final JComponent[] message = {
			new JLabel( img, SwingConstants.CENTER ),
			name,
			jtext,
		};

		final Object[] options1 = { "License", "Close" };

		// Version 1
//		JOptionPane.showConfirmDialog(parent, message, "About JayMo-Edit", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE);

		// Version 2
//		final int result = JOptionPane.showOptionDialog(parent, message, "About JayMo-Edit", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options1, 0);
//		if(result == JOptionPane.YES_OPTION)
//			Dialog_License.show(parent);

		// Version 3
		final JOptionPane pane = new JOptionPane( message, JOptionPane.PLAIN_MESSAGE, JOptionPane.YES_NO_OPTION, null, options1, options1[1] );
		final JDialog dialog = pane.createDialog( parent, "About " + Main_JayMo_Edit.NAME );
		dialog.setMinimumSize( new Dimension( 250, 350 ) );
		dialog.setVisible( true );

		final Object selectedValue = pane.getValue();
		if( selectedValue != null && selectedValue.equals( options1[0] ) )
			Dialog_License.show( parent );
	}

}
