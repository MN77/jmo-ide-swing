/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.refactor;

import java.util.Enumeration;

import javax.swing.event.ChangeListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Style;

import org.jaymo_lang.edit.editor.styler.styles.SECTION_TYPE;


/**
 * @author Michael Nitsche
 * @created 30.03.2021
 */
public class RefactorStyle implements Style {

	private final SECTION_TYPE type;


	public RefactorStyle( final SECTION_TYPE type ) {
		this.type = type;
	}

	public void addAttribute( final Object name, final Object value ) {}


	//------------------------------------------------------------

	public void addAttributes( final AttributeSet attributes ) {}

	public void addChangeListener( final ChangeListener l ) {}

	public boolean containsAttribute( final Object name, final Object value ) {
		return false;
	}

	public boolean containsAttributes( final AttributeSet attributes ) {
		return false;
	}

	public AttributeSet copyAttributes() {
		return null;
	}

	public Object getAttribute( final Object key ) {
		return null;
	}

	public int getAttributeCount() {
		return 0;
	}

	public Enumeration<?> getAttributeNames() {
		return null;
	}

	public String getName() {
		return null;
	}

	public AttributeSet getResolveParent() {
		return null;
	}

	public Object getType() {
		return this.type;
	}

	public boolean isDefined( final Object attrName ) {
		return false;
	}

	public boolean isEqual( final AttributeSet attr ) {
		return false;
	}

	public void removeAttribute( final Object name ) {}

	public void removeAttributes( final AttributeSet attributes ) {}

	public void removeAttributes( final Enumeration<?> names ) {}

	public void removeChangeListener( final ChangeListener l ) {}

	public void setResolveParent( final AttributeSet parent ) {}

}
