/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.refactor;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.Segment;
import javax.swing.text.Style;
import javax.swing.text.StyledDocument;

import org.jaymo_lang.edit.editor.styler.styles.SECTION_TYPE;

import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.group.Group2;
import de.mn77.base.data.group.Group3;
import de.mn77.base.data.group.Section;
import de.mn77.base.data.struct.table.type.TypeTable2;
import de.mn77.base.data.struct.table.type.TypeTable3;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 30.03.2021
 */
public class RefactorDocument implements StyledDocument {

	private final String                                     text;
	private final TypeTable3<Integer, Integer, SECTION_TYPE> table = new TypeTable3<>( Integer.class, Integer.class, SECTION_TYPE.class );


	public RefactorDocument( final String text ) {
		this.text = text;
	}

	public void addDocumentListener( final DocumentListener listener ) {
		Err.todo();
	}

	public Style addStyle( final String nm, final Style parent ) {
		Err.todo();
		return null;
	}

	public void addUndoableEditListener( final UndoableEditListener listener ) {
		Err.todo();
	}

	public Position createPosition( final int offs ) throws BadLocationException {
		Err.todo();
		return null;
	}

	public Group3<Integer, Integer, SECTION_TYPE> getAt( int caretPosition ) {

		// If caret at the end of a word, move it 1 char to the left
		if( caretPosition >= this.text.length() )
			caretPosition--;
		else {
			final char ccp = this.text.charAt( caretPosition );
			final boolean isPart = ccp >= 'A' && ccp <= 'Z' || ccp >= 'a' && ccp <= 'z' || ccp >= '0' && ccp <= '9' || ccp == '_';
			if( !isPart )
				caretPosition--;
		}

		for( final Group3<Integer, Integer, SECTION_TYPE> g : this.table ) {
			final int start = g.o1;
			final int end = g.o1 + g.o2; // Points to next char
			if( caretPosition >= start && caretPosition < end )
				return g;
		}

		return new Group3<>( caretPosition, 1, SECTION_TYPE.LINEBREAK );
	}

	public Color getBackground( final AttributeSet attr ) {
		Err.todo();
		return null;
	}

	public Element getCharacterElement( final int pos ) {
		Err.todo();
		return null;
	}

	public Element getDefaultRootElement() {
		Err.todo();
		return null;
	}

	public Position getEndPosition() {
		Err.todo();
		return null;
	}

	public Font getFont( final AttributeSet attr ) {
		Err.todo();
		return null;
	}

	public Color getForeground( final AttributeSet attr ) {
		Err.todo();
		return null;
	}


	//########################################################################################

	public int getLength() {
		return this.text.length();
	}

	public Style getLogicalStyle( final int p ) {
		Err.todo();
		return null;
	}

	public ArrayList<Section> getOccurences( final REFACTOR type, final String word ) {
		final ArrayList<Section> list = new ArrayList<>();
		final int wordLen = word.length();

		if( type == REFACTOR.VARIABLE )
			for( final Group3<Integer, Integer, SECTION_TYPE> row : this.table )
				if( row.o3 == SECTION_TYPE.VAR ) {
					final String part = this.text.substring( row.o1, row.o1 + row.o2 );
					if( part.equals( word ) )
						list.add( new Section( row.o1, row.o2 ) );
				}
		if( type == REFACTOR.CONSTANT )
			for( final Group3<Integer, Integer, SECTION_TYPE> row : this.table )
				if( row.o3 == SECTION_TYPE.CONST ) {
					final String part = this.text.substring( row.o1, row.o1 + row.o2 );
					if( part.equals( word ) )
						list.add( new Section( row.o1, row.o2 ) );
				}
		if( type == REFACTOR.FUNCTION )
			for( final Group3<Integer, Integer, SECTION_TYPE> row : this.table )
				if( row.o3 == SECTION_TYPE.FUNCTION || row.o3 == SECTION_TYPE.LOOP_FUNCTION || row.o3 == SECTION_TYPE.DEFINITION ) {
					final String part = this.text.substring( row.o1, row.o1 + row.o2 );

					if( part.matches( ".*\\b" + word + "\\b.*" ) ) {
						int offset = 0;

						while( offset > -1 ) {
							offset = part.indexOf( word, offset );

							if( offset > -1 ) {
								if( offset + wordLen == part.length() || part.substring( offset ).matches( word + "\\b.*" ) )
									list.add( new Section( row.o1 + offset, word.length() ) );
								offset += wordLen;
							}
						}
					}
				}

		return list;
	}

	public Element getParagraphElement( final int pos ) {
		Err.todo();
		return null;
	}

	public Object getProperty( final Object key ) {
		Err.todo();
		return null;
	}

	public Element[] getRootElements() {
		Err.todo();
		return null;
	}

	public Position getStartPosition() {
		Err.todo();
		return null;
	}

	public Style getStyle( final String nm ) {
		Err.todo();
		return null;
	}

	public String getText( final int offset, final int length ) throws BadLocationException {
		if( offset != 0 || length != this.text.length() )
			Err.todo( offset, length, this.text.length() );
		return this.text;
	}

	public void getText( final int offset, final int length, final Segment txt ) throws BadLocationException {
		Err.todo();
	}

	public void insertString( final int offset, final String str, final AttributeSet a ) throws BadLocationException {
		Err.todo();
	}

	public void putProperty( final Object key, final Object value ) {
		Err.todo();
	}

	public void remove( final int offs, final int len ) throws BadLocationException {
		Err.todo();
	}

	public void removeDocumentListener( final DocumentListener listener ) {
		Err.todo();
	}

	public void removeStyle( final String nm ) {
		Err.todo();
	}

	public void removeUndoableEditListener( final UndoableEditListener listener ) {
		Err.todo();
	}

	public String renameConst( final String oldName, final String newName ) {
		final StringBuilder sb = new StringBuilder();

		for( final Group3<Integer, Integer, SECTION_TYPE> row : this.table ) {
			String part = this.text.substring( row.o1, row.o1 + row.o2 );
			if( row.o3 == SECTION_TYPE.CONST && part.equals( oldName ) )
				part = newName;
			sb.append( part );
		}
		return sb.toString();
	}

	public String renameFunction( final String oldName, final String newName ) {
		final StringBuilder sb = new StringBuilder();

		for( final Group3<Integer, Integer, SECTION_TYPE> row : this.table ) {
			String part = this.text.substring( row.o1, row.o1 + row.o2 );
			if( row.o3 == SECTION_TYPE.FUNCTION || row.o3 == SECTION_TYPE.LOOP_FUNCTION )
				part = this.iRenameFunction( part, oldName, newName );

			if( row.o3 == SECTION_TYPE.DEFINITION ) {
				final Group2<String, String> def = this.iSplitDefinition( part );
				if( def.o2.equals( oldName ) )
					part = def.o1 + newName;
			}
			sb.append( part );
		}
		return sb.toString();
	}

	public String renameType( final String oldName, final String newName ) {
		final StringBuilder sb = new StringBuilder();

		for( final Group3<Integer, Integer, SECTION_TYPE> row : this.table ) {
			String part = this.text.substring( row.o1, row.o1 + row.o2 );
			if( row.o3 == SECTION_TYPE.TYPE && part.equals( oldName ) )
				part = newName;

			if( row.o3 == SECTION_TYPE.DEFINITION ) {
				final Group2<String, String> def = this.iSplitDefinition( part );
				if( def.o2.equals( oldName ) )
					part = def.o1 + newName;
			}
			sb.append( part );
		}
		return sb.toString();
	}

	public String renameVar( final String oldName, final String newName ) {
		final StringBuilder sb = new StringBuilder();

		for( final Group3<Integer, Integer, SECTION_TYPE> row : this.table ) {
			String part = this.text.substring( row.o1, row.o1 + row.o2 );
			if( row.o3 == SECTION_TYPE.VAR && part.equals( oldName ) )
				part = newName;
			sb.append( part );
		}
		return sb.toString();
	}

	public void render( final Runnable r ) {
		Err.todo();
	}

	public void setCharacterAttributes( final int offset, final int length, final AttributeSet s, final boolean replace ) {
		this.table.add( offset, length, ((RefactorStyle)s).getType() );
	}

	public void setLogicalStyle( final int pos, final Style s ) {
		Err.todo();
	}

	public void setParagraphAttributes( final int offset, final int length, final AttributeSet s, final boolean replace ) {
		Err.todo();
	}

	public void show() {
		MOut.print( this.table.toDescribe() );
	}

	private String iRenameFunction( final String part, final String oldName, final String newName ) {
		final TypeTable2<String, Boolean> tab = ConvertString.splitToTable( part, ".", ":" );
		final StringBuilder sb = new StringBuilder();
		for( final Group2<String, Boolean> row : tab )
			if( row.o2 )
				sb.append( row.o1 );
			else
				sb.append( row.o1.equals( oldName ) ? newName : row.o1 );
		return sb.toString();
	}

	private Group2<String, String> iSplitDefinition( final String def ) {
		int gap = 0;
		for( final char c : def.toCharArray() )
			if( ": \t".indexOf( c ) > -1 )
				gap++;
			else
				break;

		return new Group2<>( def.substring( 0, gap ), def.substring( gap ) );
	}

}
