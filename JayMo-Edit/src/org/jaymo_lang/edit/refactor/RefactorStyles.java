/*******************************************************************************
 * Copyright (C): 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.refactor;

import org.jaymo_lang.edit.editor.styler.styles.SECTION_TYPE;
import org.jaymo_lang.edit.editor.styler.styles.Styles;


/**
 * @author Michael Nitsche
 * @created 28.10.2020
 */
public class RefactorStyles extends Styles {

	public RefactorStyles() {
		super();

		for( final SECTION_TYPE st : SECTION_TYPE.values() )
			this.styles[st.ordinal()] = new RefactorStyle( st );
	}

}
