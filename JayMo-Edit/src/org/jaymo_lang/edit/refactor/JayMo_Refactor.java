/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.refactor;

import java.awt.Component;
import java.util.ArrayList;

import org.jaymo_lang.edit.editor.styler.Scanner;
import org.jaymo_lang.edit.editor.styler.styles.SECTION_TYPE;
import org.jaymo_lang.edit.lib.Lib_IDE;
import org.jaymo_lang.util.Lib_Comply;

import de.mn77.base.data.constant.CONDITION;
import de.mn77.base.data.group.Group3;
import de.mn77.base.data.group.Group4;
import de.mn77.base.data.group.Section;
import de.mn77.lib.swing.DIALOG_TYPE;
import de.mn77.lib.swing.Lib_SwingDialog;


/**
 * @author Michael Nitsche
 * @created 29.03.2021
 *
 *          TODO: Werden beim Refactoring am Ende Leerzeilen gelöscht?
 */
public class JayMo_Refactor {

	public static ArrayList<Section> occurrences( final String text, final int caretPosition ) {
		final Group3<RefactorDocument, String, REFACTOR> ssr = JayMo_Refactor.iScanAndSearch( text, caretPosition );
		if( ssr == null )
			return null;
		return ssr.o1.getOccurences( ssr.o3, ssr.o2 );
	}

	public static String rename( final Component parent, final String text, final int caretPosition ) {
		final Group3<RefactorDocument, String, REFACTOR> ssr = JayMo_Refactor.iScanAndSearch( text, caretPosition );
		if( ssr == null )
			return null;

		if( ssr.o3 == REFACTOR.TYPE ) {
			final String newName = Lib_SwingDialog.input( parent, "Refactoring type", "New name for type", ssr.o2 );
			if( newName == null )
				return null;

			if( !Lib_Comply.checkTypeName( newName ) ) {
				Lib_SwingDialog.okay( parent, DIALOG_TYPE.ERROR, "Name convention error", "Invalid name for type: " + newName );
				return null;
			}
			return newName == null ? null : ssr.o1.renameType( ssr.o2, newName );
		}

		if( ssr.o3 == REFACTOR.FUNCTION ) {
			final String newName = Lib_SwingDialog.input( parent, "Refactoring function", "New name for function", ssr.o2 );
			if( newName == null )
				return null;

			if( !Lib_Comply.checkFunctionName( newName, CONDITION.MAYBE ) ) {
				Lib_SwingDialog.okay( parent, DIALOG_TYPE.ERROR, "Name convention error", "Invalid name for function: " + newName );
				return null;
			}

			//TODO: Geltungsbereich!!!
			return newName == null ? null : ssr.o1.renameFunction( ssr.o2, newName );
		}

		if( ssr.o3 == REFACTOR.VARIABLE ) {
			final String newName = Lib_SwingDialog.input( parent, "Refactoring variable", "New name for variable", ssr.o2 );
			if( newName == null )
				return null;

			if( !Lib_Comply.checkVarName( newName ) && !Lib_Comply.checkConstName( newName ) ) {
				Lib_SwingDialog.okay( parent, DIALOG_TYPE.ERROR, "Name convention error", "Invalid name for variable: " + newName );
				return null;
			}

			//TODO: Geltungsbereich!!!
			return newName == null ? null : ssr.o1.renameVar( ssr.o2, newName );
		}

		if( ssr.o3 == REFACTOR.CONSTANT ) {
			final String newName = Lib_SwingDialog.input( parent, "Refactoring constant", "New name for constant", ssr.o2 );
			if( newName == null )
				return null;

			if( !Lib_Comply.checkVarName( newName ) && !Lib_Comply.checkConstName( newName ) ) {
				Lib_SwingDialog.okay( parent, DIALOG_TYPE.ERROR, "Name convention error", "Invalid name for type: " + newName );
				return null;
			}

			//TODO: Geltungsbereich!!!
			return newName == null ? null : ssr.o1.renameConst( ssr.o2, newName );
		}

//		if(g3.o1 == PART_TYPE.EVENT) {
//			String newName = Lib_SwingDialog.input(parent, "Refactoring event", "New name for event", word);
//			//TODO: Definition, Geltungsbereich!!!
//			return g1.o4.renameEvent(word, newName );
//		}

//		MOut.temp(scanResult.o3);
		return null;
	}

	private static boolean iForbidden( final String word ) {
		return Lib_IDE.isMagicVariable( word ) || Lib_IDE.isAtomicWord( word );
	}

	private static REFACTOR iGetType( final String word, final SECTION_TYPE sectionType ) {
		if( word.length() == 0 || JayMo_Refactor.iForbidden( word ) )
			return REFACTOR.UNKNOWN;

		final char c0 = word.charAt( 0 );
		final boolean firstUp = c0 >= 'A' && c0 <= 'Z';

		if( word.equals( word.toUpperCase() ) )
			return firstUp ? REFACTOR.CONSTANT : REFACTOR.UNKNOWN;
		if( firstUp )
			return REFACTOR.TYPE;
		if( sectionType == SECTION_TYPE.DEFINITION )
			return REFACTOR.FUNCTION;
		return c0 >= 'a' && c0 <= 'z'
			? sectionType == SECTION_TYPE.FUNCTION || sectionType == SECTION_TYPE.LOOP_FUNCTION
				? REFACTOR.FUNCTION
				: REFACTOR.VARIABLE
			: REFACTOR.UNKNOWN;
	}

	private static Group3<RefactorDocument, String, REFACTOR> iScanAndSearch( final String text, final int caretPosition ) {
		final Group4<Integer, Integer, SECTION_TYPE, RefactorDocument> scanResult = JayMo_Refactor.scan( text, caretPosition );

		switch( scanResult.o3 ) {
			case FUNCTION:
			case LOOP_FUNCTION:
			case VAR:
			case CONST:
			case TYPE:
			case EVENT:
			case DEFINITION:
				break;
			default:
				return null;
		}

		final String textpart = text.substring( scanResult.o1, scanResult.o1 + scanResult.o2 );
		final int start = caretPosition - scanResult.o1;

		final Group3<REFACTOR, Integer, Integer> g3 = JayMo_Refactor.iSearchType( textpart, start, scanResult.o3 );
		final String word = textpart.substring( g3.o2, g3.o3 );
		if( g3.o1 == REFACTOR.UNKNOWN || word.length() == 0 )
			return null;

		return new Group3<>( scanResult.o4, word, g3.o1 );
	}

	private static int iSearchEnd( final String text, final int start ) {

		for( int i = start; i < text.length(); i++ ) {
			final char c = text.charAt( i );
			if( c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '_' )
				continue;
			else
				return i;
		}
		return text.length();
	}

	private static Group3<REFACTOR, Integer, Integer> iSearchType( final String text, final int caretPosition, final SECTION_TYPE sectionType ) {

		for( int i = caretPosition - 1; i >= 0; i-- ) {
			final char c = text.charAt( i );

			if( c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' )
				continue;
			else if( c >= '0' && c <= '9' || c == '_' )
				continue;
			else {
				final int end = JayMo_Refactor.iSearchEnd( text, i + 1 );
				final String word = text.substring( i + 1, end );
				final REFACTOR rt = JayMo_Refactor.iGetType( word, sectionType );
				return new Group3<>( rt, i + 1, end );
			}
		}

		final int end = JayMo_Refactor.iSearchEnd( text, 0 );
		final String word = text.substring( 0, end );
		final REFACTOR rt = JayMo_Refactor.iGetType( word, sectionType );
		return new Group3<>( rt, 0, end );
	}

	private static Group4<Integer, Integer, SECTION_TYPE, RefactorDocument> scan( final String text, final int caretPosition ) {
		final RefactorDocument doc = new RefactorDocument( text );
		final RefactorStyles styles = new RefactorStyles();
		final Scanner scan = new Scanner( doc, styles );
		scan.update();

		final Group3<Integer, Integer, SECTION_TYPE> g3 = doc.getAt( caretPosition );
		return new Group4<>( g3.o1, g3.o2, g3.o3, doc );
	}

}
