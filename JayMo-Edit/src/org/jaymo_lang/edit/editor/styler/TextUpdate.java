/*******************************************************************************
 * Copyright (C): 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.editor.styler;

/**
 * @author Michael Nitsche
 * @created 25.11.2020
 * @deprecated currently not used / but meaningful to scan only a little part and not the entire document
 */
@Deprecated
public class TextUpdate {

	public final int offset;
	public final int lengthOld;
	public final int lengthNew;


	public TextUpdate( final int offset, final int lenOld, final int lenNew ) {
		this.offset = offset;
		this.lengthOld = lenOld;
		this.lengthNew = lenNew;
	}

}
