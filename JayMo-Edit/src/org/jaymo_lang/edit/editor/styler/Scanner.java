/*******************************************************************************
 * Copyright (C): 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.editor.styler;

import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;

import org.jaymo_lang.edit.editor.CodeEditor;
import org.jaymo_lang.edit.editor.styler.styles.SECTION_TYPE;
import org.jaymo_lang.edit.editor.styler.styles.Styles;
import org.jaymo_lang.edit.lib.Lib_IDE;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 25.11.2020
 */
public class Scanner {

	private final StyledDocument doc;
	private final Styles         styles;

	boolean scanning = false;

	private int          start = 0;
	private SECTION_TYPE style = SECTION_TYPE.LINEBREAK;


	public Scanner( final StyledDocument doc, final Styles styles ) {
		this.doc = doc;
		this.styles = styles;
	}

	/**
	 * TODO:
	 * - braucht eigentlich nur die aktuelle Zeile scannen!?
	 * - Backslash: z.B.: '\''
	 * - Textblock, Treeblock, Tableblock
	 */
//	public void update(final TextUpdate lastUpdate) {
	public void update() {
		if( this.scanning )
			//			MOut.temp("Already scanning!"); //TODO Vorherigen Scan abbrechen!?
			return;
		this.scanning = true;

		final int len = this.doc.getLength();
		String text = null;

		try {
			text = this.doc.getText( 0, len ); // Segment benutzen! Nicht alles scannen!
		}
		catch( final BadLocationException e ) {
			MOut.temp( "Bad location", e, 0, len );
			Err.exit( e );
		}

		// Reset all variables
		this.start = 0;
		this.style = SECTION_TYPE.LINEBREAK;
		boolean lock = false;
		boolean inCommand = false;

		for( int i = 0; i < len; i++ ) {
			final char c = text.charAt( i );

			// --- Modifier ---

			if( c == '?' ) //  || c == '¿'
				continue;

			// --- Comments ---

			if( c == '#' && this.style != SECTION_TYPE.CHAR && this.style != SECTION_TYPE.STRING && this.style != SECTION_TYPE.COMMENT_BLOCK ) {

				if( this.style != SECTION_TYPE.COMMENT_LINE && i + 3 < len && text.substring( i, i + 4 ).equals( "#**#" ) ) {
					this.iCheck( text, i, SECTION_TYPE.COMMENT_SCRIPT );
					i = len;
					continue;
				}
				this.iCheck( text, i, SECTION_TYPE.COMMENT_LINE );
				continue;
			}

			if( c == '/' && i + 1 < len && text.charAt( i + 1 ) == '*' && this.style != SECTION_TYPE.CHAR && this.style != SECTION_TYPE.STRING && this.style != SECTION_TYPE.COMMAND ) {
				lock = true;
				this.iCheck( text, i, SECTION_TYPE.COMMENT_BLOCK );
				continue;
			}

			if( c == '/' && i >= 1 && text.charAt( i - 1 ) == '*' && this.style == SECTION_TYPE.COMMENT_BLOCK ) {
				lock = false;
//				this.iCheck(text, i, StyleType.IGNORE);	// Platzhalter
				continue;
			}
			if( lock && this.style == SECTION_TYPE.COMMENT_BLOCK )
				continue;

			if( c == '\n' && this.style == SECTION_TYPE.COMMENT_LINE ) {
				this.iCheck( text, i, SECTION_TYPE.LINEBREAK );
				continue;
			}

			if( this.style == SECTION_TYPE.COMMENT_LINE )
				continue;

			// --- Command ---

//			if((c == '´' || c == '`') && this.style == SECTION_TYPE.COMMAND && text.charAt(i - 1) != '\\') {
			if( (c == '´' || c == '`') && inCommand && this.style != SECTION_TYPE.CHAR && this.style != SECTION_TYPE.STRING ) { // && text.charAt(i - 1) != '\\'
				this.iCheck( text, i, SECTION_TYPE.COMMAND );
				lock = false;
				inCommand = false;
				continue;
			}

			if( (c == '´' || c == '`') && !inCommand && !lock && this.style != SECTION_TYPE.CHAR && this.style != SECTION_TYPE.STRING && this.style != SECTION_TYPE.COMMAND ) {
				this.iCheck( text, i, SECTION_TYPE.COMMAND );
				final boolean calc = text.length() > i + 1 && text.charAt( i + 1 ) == '(';
				inCommand = true;
				lock = !calc;
				continue;
			}

			if( lock && this.style == SECTION_TYPE.COMMAND ) {

				if( c == '\n' ) {
					this.iCheck( text, i, SECTION_TYPE.ERROR );
					i = len;
				}
				continue;
			}
			if( c == '(' && this.style == SECTION_TYPE.COMMAND )
				continue;

			if( c == ')' && inCommand && text.length() > i + 1 && (text.charAt( i + 1 ) == '´' || text.charAt( i + 1 ) == '`') ) {
				this.iCheck( text, i, SECTION_TYPE.COMMAND );
				continue;
			}

			// --- Char ---

			if( c == '\'' && this.style == SECTION_TYPE.CHAR && text.charAt( i - 1 ) != '\\' ) {
				lock = false;
				continue;
			}

			if( c == '\'' && !lock && this.style != SECTION_TYPE.STRING && this.style != SECTION_TYPE.CHAR ) {
				this.iCheck( text, i, SECTION_TYPE.CHAR );
				lock = true;
				continue;
			}

			if( lock && this.style == SECTION_TYPE.CHAR ) {

				if( c == '\n' ) {
					this.iCheck( text, i, SECTION_TYPE.ERROR );
					i = len;
				}
				continue;
			}

			// --- String ---

			if( c == '"' && this.style == SECTION_TYPE.STRING && text.charAt( i - 1 ) != '\\' ) {
				lock = false;
				continue;
			}

			if( c == '"' && !lock && this.style != SECTION_TYPE.STRING && this.style != SECTION_TYPE.CHAR ) {
				this.iCheck( text, i, SECTION_TYPE.STRING );

				if( text.startsWith( "\"\"\"\"", i ) ) {
					final int endIndex = text.indexOf( "\"\"\"\"", i + 4 );

					if( endIndex > -1 )
						i = endIndex + 3;
					else { // Block with open end
						this.iCheck( text, i, SECTION_TYPE.ERROR );
						i = len;
					}
				}
				else
					lock = true;

				continue;
			}

			if( lock && this.style == SECTION_TYPE.STRING ) {

				if( c == '\n' ) { // String with open end
					this.iCheck( text, i, SECTION_TYPE.ERROR );
					i = len;
				}
				continue;
			}

			// --- Numbers ---

			if( this.style == SECTION_TYPE.NUMBER ) {

				if( "_abcdefABCDEFxosilz".indexOf( c ) > -1 ) {
					// _, a-f, A-F, xob, bsilfd

					if( (c == 'e' || c == 'E') && i + 1 < len ) {
						final char c1 = text.charAt( i + 1 );
						if( c1 == '+' || c1 == '-' )
							i++;
					}
					continue;
				}
				// + - bleiben schwarz (Funktion)	// after e/E it's not correct!

				if( c == '.' && i + 1 < len ) {
					final char c1 = text.charAt( i + 1 );
					if( c1 >= '0' && c1 <= '9' )
						continue;
				}
			}

			if( c >= '0' && c <= '9' && this.style != SECTION_TYPE.UNKNOWN_WORD && this.style != SECTION_TYPE.FUNCTION && this.style != SECTION_TYPE.DEFINITION ) {
				this.iCheck( text, i, SECTION_TYPE.NUMBER ); // + - bleiben schwarz (Funktion)
				continue;
			}

			// --- Whitespace, Linebreaks, Tabs, ... ---

			if( c == '\n' ) {
				this.iCheck( text, i, SECTION_TYPE.LINEBREAK );
				continue;
			}

			if( c == '\t' ) {
				this.iCheck( text, i, SECTION_TYPE.TABULATOR );
				continue;
			}

			if( c == ' ' && this.style == SECTION_TYPE.LINEBREAK ) {
				this.iCheck( text, i, SECTION_TYPE.WHITESPACE_LEADING );
				continue;
			}
			if( c == ' ' && this.style == SECTION_TYPE.WHITESPACE_LEADING )
				continue;

			if( c == ' ' ) {
				if( this.style != SECTION_TYPE.DEFINITION && this.style != SECTION_TYPE.PARSERSETTING )
					this.iCheck( text, i, SECTION_TYPE.WHITESPACE );
				continue;
			}

			// --- ParserSetting ---
			if( (c == '\n' || c == '(' || c == '=') && this.style == SECTION_TYPE.PARSERSETTING ) {
				this.iCheck( text, i, c == '\n' ? SECTION_TYPE.LINEBREAK : SECTION_TYPE.BRACKET );
				continue;
			}

			if( c == '>' && this.style == SECTION_TYPE.LINEBREAK ) {
				this.iCheck( text, i, SECTION_TYPE.PARSERSETTING );
				continue;
			}
			if( this.style == SECTION_TYPE.PARSERSETTING )
				continue;

			// --- Definition ---
			if( c == ':' ) {
				if( i + 1 < len && text.charAt( i + 1 ) == ':' ) // Also next char
					this.iCheck( text, i, SECTION_TYPE.DEFINITION );
				continue;
			}

			if( c == '!' ) {
				if( i + 1 < len && text.charAt( i + 1 ) == '!' ) // Also next char
					this.iCheck( text, i, SECTION_TYPE.DEFINITION );
				continue;
			}
			if( this.style == SECTION_TYPE.DEFINITION && (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c >= '0' && c <= '9' || c == '_' || c == '@' || c == ' ' || c == '|') )
				continue;

			// --- Words, Functions, Magic ---

			if( this.style == SECTION_TYPE.FUNCTION && (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c >= '0' && c <= '9' || c == '_') )
				continue;

			if( c == '.' ) {

				if( i + 1 < len && text.charAt( i + 1 ) == '.' ) { // Also next
					this.iCheck( text, i, SECTION_TYPE.IGNORE ); // Range
					i++;
					continue;
				}
				this.iCheck( text, i, SECTION_TYPE.FUNCTION );
				continue;
			}

			if( this.style == SECTION_TYPE.MAGIC_SHORT ) {
				this.iCheck( text, i, SECTION_TYPE.FUNCTION );
				continue;
			}

			if( c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c == '_' || c == '@' ) {
				this.iCheck( text, i, SECTION_TYPE.UNKNOWN_WORD );
				continue;
			}
			if( this.style == SECTION_TYPE.UNKNOWN_WORD && (c >= '0' && c <= '9' || c == '?') ) //  || c == '¿'
				continue;

			if( this.style == SECTION_TYPE.UNKNOWN_WORD && c == '{' ) {
				final int lineEnd = text.indexOf( '\n', i );
				final int closing = text.indexOf( '}', i );

				if( closing == -1 || lineEnd >= 0 && lineEnd < closing )
					this.iCheck( text, i, SECTION_TYPE.ERROR );
				else {
					i = closing + 1;
					this.iCheck( text, i, SECTION_TYPE.IGNORE );
				}
				continue;
			}

			// --- Everything else ---

			if( Lib_IDE.SHORTCUTS.indexOf( c ) > -1 ) {

				if( this.style != SECTION_TYPE.LINEBREAK
					&& this.style != SECTION_TYPE.TABULATOR
					&& this.style != SECTION_TYPE.BRACKET
					&& this.style != SECTION_TYPE.PUNCTUATION ) {
					this.iCheck( text, i, SECTION_TYPE.IGNORE );
					continue;
				}
				this.iCheck( text, i, SECTION_TYPE.MAGIC_SHORT );

				if( i + 1 < len ) {
					final char c1 = text.charAt( i + 1 );
					if( Lib_IDE.SHORTCUTS2.indexOf( c1 ) >= 0 ) // $$ °°
						i++;
				}
				continue;
			}

			if( ";,".indexOf( c ) > -1 ) {
				this.iCheck( text, i, SECTION_TYPE.PUNCTUATION );
				continue;
			}

			if( Lib_IDE.BRACKETS.indexOf( c ) > -1 ) {
				this.iCheck( text, i, SECTION_TYPE.BRACKET );
				continue;
			}

			this.iCheck( text, i, SECTION_TYPE.IGNORE );
		}

		// Clear buffer
		this.iCheck( text, len, SECTION_TYPE.LINEBREAK ); // null

		if( CodeEditor.DEMO )
			MOut.print( "-------------------------------" );
		this.scanning = false;
	}

	private void iCheck( final String text, final int offset, final SECTION_TYPE next ) {

		if( this.style != next ) {
			final int length = offset - this.start;

			if( this.style == SECTION_TYPE.UNKNOWN_WORD )
				this.style = this.iCheckWord( text.substring( this.start, offset ) );

			if( this.style == SECTION_TYPE.FUNCTION )
				switch( text.substring( this.start, offset ) ) {
					case ".each":
					case ".times":
						this.style = SECTION_TYPE.LOOP_FUNCTION;
					default:
				}

			if( length > 0 ) {
				if( CodeEditor.DEMO )
					MOut.print( "Doc.setStyle " + this.start + " " + length + " " + this.style.name() );
				this.doc.setCharacterAttributes( this.start, length, this.styles.styles[this.style.ordinal()], true ); // true?!?
			}

			this.start = offset;
			this.style = next;
		}
	}

	private SECTION_TYPE iCheckWord( final String s ) {
//		MOut.print(s);
		final char c0 = s.charAt( 0 );

		if( Lib_IDE.isAtomicWord( s ) )
			return SECTION_TYPE.ATOMIC_LITERAL;

		if( Lib_IDE.isMagicVariable( s ) )
			return SECTION_TYPE.MAGIC_LONG;

		if( s.equals( s.toUpperCase() ) )
			if( c0 == '_' )
				return s.length() >= 2 && s.charAt( 1 ) == '_'
					? SECTION_TYPE.MAGIC_LONG
					: SECTION_TYPE.TYPE;
			else
				return SECTION_TYPE.CONST;

		if( c0 >= 'A' && c0 <= 'Z' || c0 == '_' )
			return SECTION_TYPE.TYPE;

		return SECTION_TYPE.VAR;
	}

}
