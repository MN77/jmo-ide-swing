/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.editor.funcs;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import org.jaymo_lang.edit.editor.CodeEditor;

import de.mn77.base.data.group.Section;


/**
 * @author Michael Nitsche
 * @created 2021-03-17
 *
 *          Löscht bei STRG+d die aktuelle Zeile
 */
public class Fn_DelLine {

	public void init( final CodeEditor editor ) {
		editor.addKeyListener( new KeyListener() {

			public void keyPressed( final KeyEvent e ) {}

			public void keyReleased( final KeyEvent e ) {
				if( e.getModifiersEx() == InputEvent.CTRL_DOWN_MASK && e.getKeyCode() == 68 ) // STRG + d
					Fn_DelLine.this.iRemoveLines( editor );
			}

			public void keyTyped( final KeyEvent e ) {}

		} );
	}

	private void iRemoveLines( final CodeEditor tp ) {
		final int cursorPos = tp.getCaretPosition();
		final int x = tp.getCaret().getMark();

		final Section selection = Lib_EditFunctions.getLines( tp.getText(), cursorPos, x );

		if( selection != null ) {
			tp.select( selection.start, selection.end + 1 );
			tp.replaceSelection( "" );
			tp.getHistory().add();
		}
	}

}
