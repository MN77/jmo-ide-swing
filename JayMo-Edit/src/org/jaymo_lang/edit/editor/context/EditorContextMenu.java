/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-Edit <https://www.jaymo-lang.org>.
 *
 * JayMo-Edit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-Edit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-Edit. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.edit.editor.context;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import org.jaymo_lang.edit.action.EVENTS;
import org.jaymo_lang.edit.action.JayMo_Actions;
import org.jaymo_lang.edit.editor.CodeEditor;


/**
 * @author Michael Nitsche
 * @created 29.03.2021
 */
public class EditorContextMenu extends JPopupMenu {

	private static final long serialVersionUID = -3044248699038465912L;

	private final Clipboard     clipboard;
	private final CodeEditor    editor;
	private final JayMo_Actions actions;

	private final JMenuItem undo, redo, cut, copy, paste, selectAll, selectNone, refactoring;


	public static void addTo( final CodeEditor textComponent, final JayMo_Actions actions ) {
		final EditorContextMenu m = new EditorContextMenu( textComponent, actions );
		textComponent.setComponentPopupMenu( m );
		textComponent.setInheritsPopupMenu( true );

		m.addPopupMenuListener( new PopupMenuListener() {

			public void popupMenuCanceled( final PopupMenuEvent e ) {}

			public void popupMenuWillBecomeInvisible( final PopupMenuEvent e ) {}

			public void popupMenuWillBecomeVisible( final PopupMenuEvent e ) {
				m.update();
			}

		} );
	}

	private EditorContextMenu( final CodeEditor editor, final JayMo_Actions actions ) {
		this.clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		this.editor = editor;
		this.actions = actions;

		this.undo = this.addItem( "Undo", EVENTS.UNDO, KeyEvent.VK_Z, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx() );
		this.redo = this.addItem( "Redo", EVENTS.REDO, KeyEvent.VK_Y, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx() );
		this.add( new JSeparator() );
		this.cut = this.addItem( "Cut", EVENTS.CUT, KeyEvent.VK_X, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx() );
		this.copy = this.addItem( "Copy", EVENTS.COPY, KeyEvent.VK_C, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx() );
		this.paste = this.addItem( "Paste", EVENTS.PASTE, KeyEvent.VK_V, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx() );
		this.add( new JSeparator() );
		this.selectAll = this.addItem( "Select All", EVENTS.SELECT_ALL, KeyEvent.VK_A, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx() );
		this.selectNone = this.addItem( "Select None", EVENTS.SELECT_NONE, KeyEvent.VK_A, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx() + InputEvent.SHIFT_DOWN_MASK );
		this.add( new JSeparator() );
		this.refactoring = this.addItem( "Refactoring", EVENTS.REFACTORING, KeyEvent.VK_R, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx() );
	}

	private JMenuItem addItem( final String title, final EVENTS event, final int key, final int keyMask ) {
		final JMenuItem item = new JMenuItem( title );
		item.setEnabled( false );
		item.setAccelerator( KeyStroke.getKeyStroke( key, keyMask ) );
		item.addActionListener( ev -> this.actions.perform( event ) );
		return this.add( item );
	}

	private void update() {
		this.editor.requestFocus();

		final String selectedText = this.editor.getSelectedText();
		final String text = this.editor.getText();

		final boolean enableCopyCut = selectedText != null && selectedText.length() > 0;
		final boolean enablePaste = this.clipboard.isDataFlavorAvailable( DataFlavor.stringFlavor ) && this.editor.isEnabled();
		final boolean textAvailable = text != null && text.length() > 0;
		final boolean hasSelection = this.editor.getSelectionStart() != this.editor.getSelectionEnd();

		this.cut.setEnabled( enableCopyCut );
		this.copy.setEnabled( enableCopyCut );
		this.paste.setEnabled( enablePaste );
		this.selectAll.setEnabled( textAvailable );
		this.selectNone.setEnabled( hasSelection );
		this.selectNone.setVisible( hasSelection );
		this.refactoring.setEnabled( textAvailable );

		this.undo.setEnabled( this.editor.getHistory().canUndo() );
		this.redo.setEnabled( this.editor.getHistory().canRedo() );
	}

}
